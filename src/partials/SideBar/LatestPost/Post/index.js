import React from "react";
import {Link} from "react-router-dom";

const Post = (props) => {
  return (
    <div className="item d-flex align-items-center">
      <div className="image">
        <Link to="/">
          <img src="/assets/img/small-thumbnail-1.jpg" alt="Alberto Savoia Can Teach You About" className="img-fluid" />
        </Link>
      </div>
      <div className="title">
        <Link to="/">
          <strong>Alberto Savoia Can Teach You About</strong>
        </Link>
        <div className="d-flex align-items-center">
          <div className="views"><i className="icon-eye" /> 500</div>
          <div className="comments"><i className="icon-comment" />12</div>
        </div>
      </div>
    </div>
  );
};

export default Post;
