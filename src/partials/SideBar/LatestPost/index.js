import React from "react";

import Post from "./Post";

const LatestPost = (props) => {
  return (
    <div className="widget latest-posts">
      <header>
        <h3 className="h6">Latest Posts</h3>
      </header>
      <div className="blog-posts">
        <Post />
      </div>
    </div>
  );
};

export default LatestPost;
