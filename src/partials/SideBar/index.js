import React from "react";
import PropTypes from "prop-types";

import Search from "./Search";
import LatestPost from "./LatestPost";
import Categories from "./Categories";
import Tags from "./Tags";

const SideBar = (props) => {
  return (
    <aside className="col-lg-4">
      <Search />
      <LatestPost />
      <Categories />
      <Tags />
    </aside>
  )
};

export default SideBar;
