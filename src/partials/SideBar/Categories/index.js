import React from "react";
import {Link} from "react-router-dom";

const Categories = (props) => {
  return (
    <div className="widget categories">
      <header>
        <h3 className="h6">Categories</h3>
      </header>
      <div className="item d-flex justify-content-between">
        <Link to="#">Growth</Link><span>12</span>
      </div>
      <div className="item d-flex justify-content-between">
        <Link to="#">Local</Link><span>25</span>
      </div>
      <div className="item d-flex justify-content-between">
        <Link to="#">Sales</Link><span>8</span>
      </div>
      <div className="item d-flex justify-content-between">
        <Link to="#">Tips</Link><span>17</span>
      </div>
      <div className="item d-flex justify-content-between">
        <Link to="#">Local</Link><span>25</span>
      </div>
    </div>
  );
};

export default Categories;
