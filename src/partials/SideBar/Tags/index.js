import React from "react";
import {Link} from "react-router-dom";

const Tags = (props) => {
  return (
    <div className="widget tags">
      <header>
        <h3 className="h6">Tags</h3>
      </header>
      <ul className="list-inline">
        <li className="list-inline-item">
          <Link to="#" className="tag">#Business</Link>
        </li>
        <li className="list-inline-item">
          <Link to="#" className="tag">#Technology</Link>
        </li>
        <li className="list-inline-item">
          <Link to="#" className="tag">#Fashion</Link>
        </li>
        <li className="list-inline-item">
          <Link to="#" className="tag">#Sports</Link>
        </li>
        <li className="list-inline-item">
          <Link to="#" className="tag">#Economy</Link>
        </li>
      </ul>
    </div>
  );
};

export default Tags;
