import React from "react";
import {NavLink, Link} from "react-router-dom";

const Header = () => {
  return (
    <header className="header">
      <nav className="navbar navbar-expand-lg">
        <div className="search-area">
          <div className="search-area-inner d-flex align-items-center justify-content-center">
            <div className="close-btn"><i className="icon-close"/></div>
            <div className="row d-flex justify-content-center">
              <div className="col-md-8">
                <form action="#">
                  <div className="form-group">
                    <input type="search" name="search" id="search" placeholder="What are you looking for?"/>
                    <button type="submit" className="submit">
                      <i className="icon-search-1"/>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="navbar-header d-flex align-items-center justify-content-between">
            <Link to="/" className="navbar-brand">Bootstrap Blog</Link>
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse"
                    aria-expanded="false" aria-label="Toggle navigation" className="navbar-toggler">
              <span/><span/><span/>
            </button>
          </div>
          <div id="navbarcollapse" className="collapse navbar-collapse">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <NavLink to="/" className="nav-link">Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/blog" className="nav-link">Blog</NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/contact" className="nav-link">Contact</NavLink>
              </li>
            </ul>
            <div className="navbar-text">
              <Link to="#" className="search-btn">
                <i className="icon-search-1"/>
              </Link>
            </div>
            <ul className="langs navbar-text">
              <Link to="#" className="active">EN</Link>
              <span/>
              <Link to="#">ES</Link>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
