import {
  REQUEST_BLOG_START,
  REQUEST_BLOG_FINISHED,
  REQUEST_BLOG_FAILED
} from "../constant";

const initialState = {
  loading: false,
  data: [],
  message: null
};

const blogReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_BLOG_START:
      return {
        ...state,
        loading: true,
        message: null
      };
    case REQUEST_BLOG_FINISHED:
      return {
        ...state,
        loading: false,
        data: action.payload,
        message: null
      }
    case REQUEST_BLOG_FAILED:
      return {
        ...state,
        loading: false,
        message: action.message
      }
    default:
      return {
        ...state
      }
  }
};

export default blogReducer;
